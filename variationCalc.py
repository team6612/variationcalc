import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
matplotlib.style.use('ggplot')

print("Loading Excel file")
with pd.ExcelFile("../150401_U3088P16_Pterostilbene_exp8_RR_Exporta.xls") as xls:
    datas = pd.read_excel(xls, ['data', 'Data List'], skiprows=1, header=0, index_col=0, usecols=range(2,11))

print("Start to calc variations")
for k, v in datas.items():
    print()
    print("Measurement of Variations for " + k)
    n = len(datas[k].index)
    datas[k].R = datas[k].max() - datas[k].min()
    datas[k].IQR = datas[k].quantile(0.75) - datas[k].quantile(0.25)
    datas[k].D = (datas[k] - datas[k].mean()).abs()
    datas[k].AD = datas[k].D.sum() / n
    datas[k].MAD = (datas[k] - datas[k].median()).abs().median()
    datas[k].SS = ((datas[k] - datas[k].mean()) ** 2).sum() / (n - 1)
    datas[k].S = datas[k].SS.apply(np.sqrt)
    datas[k].CV = datas[k].S / datas[k].mean() * 100
    print(pd.concat([datas[k].R, datas[k].IQR, datas[k].AD, \
                    datas[k].MAD, datas[k].SS, datas[k].S, datas[k].CV] \
                    , axis=1, keys=['R','IQR','AD','MAD','SS','S','CV']))

datas['data'].plot()
datas['Data List'].plot()

plt.show()
